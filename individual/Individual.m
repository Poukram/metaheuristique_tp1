classdef Individual
    %INDIVIDUAL Stores information about an individual to be used later for
    %data analysis
    
    properties
        x_chrom
        y_chrom
        obj_val
        fitness
        selec_proba
    end
    
    methods
        function obj = Individual(x, y)
            %INDIVIDUAL Construct an instance of this class
            %   Detailed explanation goes here
            if nargin > 0
                obj.x_chrom = x;
                obj.y_chrom = y;
                obj.obj_val = 0;
                obj.fitness = 0;
                obj.selec_proba = 0;
            end
        end
    end
end

