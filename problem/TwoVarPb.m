classdef TwoVarPb
    %TWO_VAR_PB Stores information about the problems to solve
    
    properties
        pb_name
        pb_type % 0 for minimization 1 for maximization
        x_range
        y_range
        rosenbrock = @(x, y)-((1 - x)^2 + 100*((y - x^2)^2));
        griewank = @(x, y)(x^2 + y^2) / 4000 - cos(x) * cos(y/sqrt(2)) + 1;
        fun
    end
    
    methods
        function obj = TwoVarPb(name)
            if (strcmp(name, "rosenbrock"))
                obj.pb_name = "Maximization of the negative Rosenbrock function";
                obj.pb_type = 1;
                obj.x_range = [0 2];
                obj.y_range = [0 3];
                obj.fun = obj.rosenbrock;
            elseif (strcmp(name, "griewank"))
                obj.pb_name = "Minimization of the Griewank function";
                obj.pb_type = 0;
                obj.x_range = [-30 30];
                obj.y_range = [-30 30];
                obj.fun = obj.griewank;
            else
                disp("Invalid problem name")
            end
        end
    end
end

