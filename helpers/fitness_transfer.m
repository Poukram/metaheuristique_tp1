function pop = fitness_transfer(pop, popsize, pb_type)
%FITNESS_TRANSFER Summary of this function goes here
%   Detailed explanation goes here

if (pb_type == 0)
    max_fitness = max([pop.fitness]);
    for n = 1:popsize
        pop(n).fitness = max_fitness - pop(n).fitness;
    end
else
    min_fitness = min([pop.fitness]);
    if (min_fitness < 0)
        for n = 1:popsize
            pop(n).fitness = pop(n).fitness - min_fitness;
        end
    end
end

