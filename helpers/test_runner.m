function test_runner(op, p)
%TEST_RUNNER Summary of this function goes here
%   tables store: name of params, operators, best stopping gen, mean
%   stopping gen, variance stopping gen, best obj_val, mean obj val, var obj val,
%   best x, best y

test_nb = 50; % number of times to run a single configuration
op_nb = size(op{1});
p_nb = size(p{1});
op_nb = op_nb(2);
p_nb = p_nb(2);
cell_nb = 10; % number of cells in a table
excell_file = "test_data.xlsx";
var_names = {"Parametres", "Operateurs", "Meilleure gen. max", "Gen. max moyenne",...
    "Variance gen. max", "Meilleure valeur obj.", "Valeur obj. moyenne",...
    "Variance valeur obj.", "Meilleur x", "Meilleur y"};
tabl = cell(op_nb * p_nb + 1, cell_nb,2);
tabl(1,:,1) = var_names;
tabl(1,:,2) = var_names;
problem_name = ["rosenbrock", "griewank"];
config_base = ["Cfg_binaire", "Cfg_reel", "Cfg_sstate"];
p = p{1};
op = op{1};

for i = 1:2 % number of problems to test
    pb = TwoVarPb(problem_name(i));
    for n = 1:p_nb % number of parameters combinations
        res = cell(op_nb, cell_nb);
        % INSERT parfor here
        parfor k = 1:op_nb % number of operators combinations
            pn = p(n);
            opk = op(k);
            res12 = cell(test_nb, cell_nb);
            res13 = cell(1,cell_nb);
            
            if (k < 3)
                cb_name = config_base(1);
                nb = k;
            elseif (k < 5)
                cb_name = config_base(2);
                nb = k - 2;
            else
                cb_name = config_base(3);
                nb = k - 4;
            end
            
            for j = 1:test_nb
                disp("starting n="+n+" k="+k+" j="+j); 
                res111 = cell(1, cell_nb);
                res112 = genetic_a(pn.popsize, 1, pn.g_stop, pn.pc, pn.pm, opk, pb);
                
                if (pb.pb_type == 1)
                    [M, I] = max([res112.hist.obj_val]);
                else
                    [M, I] = min([res112.hist.obj_val]);
                end
                
                best_ind = res112.hist(I);
                x = best_ind.x_chrom;
                y = best_ind.y_chrom;
                
                % Check if binary coding is used
                if (res112.bc == 1)
                    x = res112.chr_b.bin_chromosome_decode(x, pb.x_range);
                    y = res112.chr_b.bin_chromosome_decode(y, pb.y_range);
                end
                
                res111{1} = "Param"+n;
                res111{2} = cb_name+nb;
                res111{3} = res112.gen_nb;
                res111{4} = 0; % tb computed later
                res111{5} = 0; % tb computed later
                res111{6} = M;
                res111{7} = 0; % tb computed later
                res111{8} = 0; % tb computed later
                res111{9} = x;
                res111{10} = y;
                
                res12(j,:) = res111;
            end
            
            % We have to judge on the objective value and problem type
            % since fitness is scale dependent
            if (pb.pb_type == 1)
                [~, I] = max([res12{:,6}]);
            else
                [~, I] = min([res12{:,6}]);
            end
            
            res13(1,:) = res12(I,:);
            
            g_mean = mean([res12{:,3}]);
            g_var = var([res12{:,3}]);
            b_val_mean = mean([res12{:,6}]);
            b_val_var = var([res12{:,6}]);
            
            res13{4} = g_mean;
            res13{5} = g_var;
            res13{7} = b_val_mean;
            res13{8} = b_val_var;
            
            res1{k,1} = res13;          
            
            res12 = {};
        end
        res1 = vertcat(res1{:});
        
        d = (n-1) * op_nb + 1 + 1; % supplementary +1 accounts for variable names
        tabl(d:d+5, :, i) = res1;
        
        res1 = {};
    end
    str_tabl = string(tabl(:,:,i));
    xlswrite(excell_file, str_tabl, problem_name(i), "A1");
end
end

