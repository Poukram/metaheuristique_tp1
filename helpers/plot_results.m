% TODO
function plot_results(pop_history, popsize, g_max, binary_coding, pb, chr_builder)
%PLOT_RESULTS A graphical representation of the genetic EA
%   One widow shows the evolution of the best known value
%   The other plots the population on the problem at each generation
%   NB: The final intended purpose is to hhave a slider for the second
%   windows in order to select the current generation being displayed
best_sol = zeros(g_max+1,1);
hist_obj_val_pop = zeros(g_max+1, popsize, 3);

for n = 1:g_max+1
    for k = 1:popsize
        x = pop_history(n,k).x_chrom;
        y = pop_history(n,k).y_chrom;
        
        if binary_coding
            x = chr_builder.bin_chromosome_decode(x, pb.x_range);
            y = chr_builder.bin_chromosome_decode(y, pb.y_range);
        end
        
        hist_obj_val_pop(n,k,:) = [x y pop_history(n,k).obj_val];
    end
end

for n = 1:g_max+1
    tmp = [pop_history(n,:).obj_val];
    
    if(pb.pb_type == 0)
        [M,I] = min(tmp);
    else
        [M,I] = max(tmp);
    end
    
    x = pop_history(n,I).x_chrom;
    y = pop_history(n,I).y_chrom;
    
    if binary_coding
        x = chr_builder.bin_chromosome_decode(x, pb.x_range);
        y = chr_builder.bin_chromosome_decode(y, pb.y_range);
    end
    
    best_sol(n) = M;
end
best_val_fig = figure;
pop_evol_fig = figure;

cur_fun_str = func2str(pb.fun);

figure(best_val_fig);
plot(0:g_max, best_sol)
title(pb.pb_name)
ylabel("Best value")
xlabel("Generation number")
legend(cur_fun_str)

figure(pop_evol_fig);

fsurf(pb.fun, [pb.x_range pb.y_range], 'ShowContours','on', 'MeshDensity',20)
title("Population location inside of problem space")

hold on

for n = 1:g_max+1
    X = hist_obj_val_pop(n,:,1);
    Y = hist_obj_val_pop(n,:,2);
    Z = hist_obj_val_pop(n,:,3);
    name = "Generation " + n;
    scatter3(X, Y, Z, 100, '+', 'LineWidth',2, 'DisplayName',name)
    hold on
end
end

