function res = test_builder(op, p)
%TEST_BUILDER Summary of this function goes here
%   Detailed explanation goes here
op(1).crossover = "single_pt";
op(2).crossover = "uniform";
op(3).crossover = "laplace";
op(4).crossover = "blend";
op(5).crossover = "uniform";
op(6).crossover = "laplace";
op(1).mutation = "bit_flip";
op(2).mutation = "bit_flip";
op(5).mutation = "bit_flip";
op(5).steady_s = 1;
op(6).steady_s = 1;

p(1).pc = 0.8;
p(2).pc = 0.8;
p(3).pc = 0.5;
p(4).pc = 0.5;
p(1).pm = 0.025;
p(2).pm = 0.1;
p(3).pm = 0.025;
p(4).pm = 0.1;

res = {op; p};
end

