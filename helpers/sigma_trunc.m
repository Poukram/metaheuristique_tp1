function pop = sigma_trunc(pop, popsize)
%SIGMA_TRUNC Summary of this function goes here
%   Detailed explanation goes here
pop_fit = [pop.fitness];
c = 2.5;
sigma = std(pop_fit);
fit_avg = mean(pop_fit);

for n = 1:popsize
    tmp = pop(n).fitness - (fit_avg - c * sigma);
    if (tmp < 0)
        tmp = 0;
    end
    pop(n).fitness = tmp;
end
end