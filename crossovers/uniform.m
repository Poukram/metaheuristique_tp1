function enfants = uniform(pop, popsize, pc)
%SINGLE_PT Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual(0, 0);
perm = randperm(popsize);
n = 0;
    function chromosome = complementary_chrom_gen(p1, p2)
        chromosome = zeros(2,2,12);
        p_sel = p1.fitness / (p1.fitness + p2.fitness);
        
        for k = 1:12
            sel = rand();
            if (sel <= p_sel)
                chromosome(1,1,k) = p1.x_chrom(k);
                chromosome(1,2,k) = p2.x_chrom(k);
            else
                chromosome(1,1,k) = p2.x_chrom(k);
                chromosome(1,2,k) = p1.x_chrom(k);
            end
        end
        
        for k = 1:12
            sel = rand();
            if (sel <= p_sel)
                chromosome(2,1,k) = p1.y_chrom(k);
                chromosome(2,2,k) = p2.y_chrom(k);
            else
                chromosome(2,1,k) = p2.y_chrom(k);
                chromosome(2,2,k) = p1.y_chrom(k);
            end
        end
    end
while(n < popsize)
    parent_1 = pop(perm(n + 1));
    parent_2 = pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        % Initialization of the children
        enfants(n + 1) = parent_1;
        enfants(n + 2) = parent_2;
        
        chromosomes = complementary_chrom_gen(parent_1, parent_2);
        
        enfants(n + 1).x_chrom = chromosomes(1,1,:);
        enfants(n + 2).x_chrom = chromosomes(1,2,:);
        
        enfants(n + 1).y_chrom = chromosomes(2,1,:);
        enfants(n + 2).y_chrom = chromosomes(2,2,:);
              
    else
        enfants(n + 1) = parent_1;
        enfants(n + 2) = parent_2;
    end
    
    n = n +2;
end
end