function enfants = blend(pop, popsize, pc)
%BLEND Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual(0, 0);
perm = randperm(popsize);
n = 0;
alpha = 0.5;

while(n < popsize)
    p1 = pop(perm(n + 1));
    p2 = pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        for k = 1:2
            g = [sort([p1.x_chrom p2.x_chrom]); sort([p1.y_chrom p2.y_chrom])];
            a_x = g(1,1) - alpha * (g(1,2) - g(1,1));
            b_x = g(1,2) + alpha * (g(1,2) - g(1,1));
            a_y = g(2,1) - alpha * (g(2,2) - g(2,1));
            b_y = g(2,2) + alpha * (g(2,2) - g(2,1));
            
            enfants(n + k).x_chrom = rand() * (b_x - a_x) + a_x;
            enfants(n + k).y_chrom = rand() * (b_y - a_y) + a_y;
        end            
    else
        enfants(n + 1) = p1;
        enfants(n + 2) = p2;
    end
    
    n = n +2;
end
end