function enfants = laplace_c(pop, popsize, pc)
%LAPLACE_C Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual(0, 0);
perm = randperm(popsize);
n = 0;
a = 0;
b = 0.3;

while(n < popsize)
    p1 = pop(perm(n + 1));
    p2 = pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        alphax = rand();
        if (alphax <= 0.5)
            betax = a - b * log(alphax);
        else
            betax = a + b * log(alphax);
        end
        
        alphay = rand();
        if (alphay <= 0.5)
            betay = a - b * log(alphay);
        else
            betay = a + b * log(alphay);
        end
        enfants(n + 1).x_chrom = p1.x_chrom + betax * abs(p1.x_chrom - p2.x_chrom);   
        enfants(n + 1).y_chrom = p1.y_chrom + betay * abs(p1.y_chrom - p2.y_chrom);
        
        enfants(n + 2).x_chrom = p2.x_chrom + betax * abs(p1.x_chrom - p2.x_chrom);   
        enfants(n + 2).y_chrom = p2.y_chrom + betay * abs(p1.y_chrom - p2.y_chrom);
    else
        enfants(n + 1) = p1;
        enfants(n + 2) = p2;
    end
    
    n = n +2;
end
end