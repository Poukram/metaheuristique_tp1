function enfants = simulated_bin(pop, popsize, pc)
%BLEND Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual(0, 0);
perm = randperm(popsize);
n = 0;
ctrl = 3.5;

    function beta = beta_compute(k)
        u = rand();
        if (u <= 0.5)
            beta = (2 * u)^(1/(k+1));
        else
            beta = (1/(2 * (1 - u)))^(1/(k+1));
        end
    end

while(n < popsize)
    p1 = pop(perm(n + 1));
    p2 = pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        b_x = beta_compute(ctrl);
        b_y = beta_compute(ctrl);
        
        enfants(n+1).x_chrom = 0.5 * (p1.x_chrom + p2.x_chrom) +...
            0.5 * b_x * (p1.x_chrom - p2.x_chrom);
        enfants(n+1).y_chrom = 0.5 * (p1.y_chrom + p2.y_chrom) +...
            0.5 * b_y * (p1.y_chrom - p2.y_chrom);
        
        enfants(n+2).x_chrom = 0.5 * (p1.x_chrom + p2.x_chrom) +...
            0.5 * b_x * (p2.x_chrom - p1.x_chrom);
        enfants(n+2).y_chrom = 0.5 * (p1.y_chrom + p2.y_chrom) +...
            0.5 * b_y * (p2.y_chrom - p1.y_chrom);
        
    else
        enfants(n + 1) = p1;
        enfants(n + 2) = p2;
    end
    
    n = n +2;
end
end