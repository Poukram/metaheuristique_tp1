function enfants = local_arithmetic(pop, popsize, pc)
%SINGLE_PT Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual(0, 0);
perm = randperm(popsize);
n = 0;

while(n < popsize)
    p1 = pop(perm(n + 1));
    p2 = pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        alpha = rand();
        beta = 1 - alpha;
        
        enfants(n + 1).x_chrom = alpha * p1.x_chrom + beta * p2.x_chrom;
        enfants(n + 1).y_chrom = alpha * p1.y_chrom + beta * p2.y_chrom;
        
        alpha = rand();
        beta = 1 - alpha;
        
        enfants(n + 2).x_chrom = alpha * p1.x_chrom + beta * p2.x_chrom;
        enfants(n + 2).y_chrom = alpha * p1.y_chrom + beta * p2.y_chrom;
            
    else
        enfants(n + 1) = p1;
        enfants(n + 2) = p2;
    end
    
    n = n +2;
end
end