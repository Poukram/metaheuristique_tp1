function enfants = single_pt(pop, popsize, pc)
%SINGLE_PT Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual(0, 0);
perm = randperm(popsize);
n = 0;

while(n < popsize)
    parent_1 = pop(perm(n + 1));
    parent_2 = pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        % First chromosome
        cut_pt = randi(11); % l - 1  / l = 12 pour codage binaire
        
        res_11 = parent_1.x_chrom(1:cut_pt);
        res_12 = parent_2.x_chrom(cut_pt+1:12);
        res_13 = parent_2.x_chrom(1:cut_pt);
        res_14 = parent_1.x_chrom(cut_pt+1:12);
        
        cut_pt = randi(11); % l - 1  / l = 12 pour codage binaire
        
        res_21 = parent_1.y_chrom(1:cut_pt);
        res_22 = parent_2.y_chrom(cut_pt+1:12);
        res_23 = parent_2.y_chrom(1:cut_pt);
        res_24 = parent_1.y_chrom(cut_pt+1:12);

        enfants(n + 1).x_chrom = [res_11, res_12];
        enfants(n + 2).x_chrom = [res_13, res_14];
        
        enfants(n + 1).y_chrom = [res_21, res_22];
        enfants(n + 2).y_chrom = [res_23, res_24];
        
    else
        enfants(n + 1) = parent_1;
        enfants(n + 2) = parent_2;
    end
    
    n = n +2;
end
end