function enfants = multi_pt(pop, popsize, pc)
%SINGLE_PT Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual(0, 0);
perm = randperm(popsize);
n = 0;
    function cut_pts = cut_ptss_gen()
        while (1)
            cut_pts = randi(11, 2, 1); % l - 1  / l = 12 pour codage binaire
            if (cut_pts(1) ~= cut_pts(2))
                break;
            end
        end
        cut_pts = sort(cut_pts);
    end

while(n < popsize)
    parent_1 = pop(perm(n + 1));
    parent_2 = pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        % First chromosome
        cut_pts = cut_ptss_gen();
        
        res_111 = parent_1.x_chrom(1:cut_pts(1));
        res_112 = parent_1.x_chrom(cut_pts(1)+1:cut_pts(2));
        res_113 = parent_1.x_chrom(cut_pts(2)+1:12);
        res_121 = parent_2.x_chrom(1:cut_pts(1));
        res_122 = parent_2.x_chrom(cut_pts(1)+1:cut_pts(2));
        res_123 = parent_2.x_chrom(cut_pts(2)+1:12);
        
        % Second chromosome
        cut_pts = cut_ptss_gen();
        
        res_211 = parent_1.y_chrom(1:cut_pts(1));
        res_212 = parent_1.y_chrom(cut_pts(1)+1:cut_pts(2));
        res_213 = parent_1.y_chrom(cut_pts(2)+1:12);
        res_221 = parent_2.y_chrom(1:cut_pts(1));
        res_222 = parent_2.y_chrom(cut_pts(1)+1:cut_pts(2));
        res_223 = parent_2.y_chrom(cut_pts(2)+1:12);
        
        enfants(n + 1).x_chrom = [res_111, res_122, res_113];
        enfants(n + 2).x_chrom = [res_121, res_112, res_123];
        
        enfants(n + 1).y_chrom = [res_211, res_222, res_213];
        enfants(n + 2).y_chrom = [res_221, res_212, res_223];
        
    else
        enfants(n + 1) = parent_1;
        enfants(n + 2) = parent_2;
    end
    
    n = n +2;
end
end