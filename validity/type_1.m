function corrected = type_1(pop, popsize, pb)
%TYPE_1 Summary of this function goes here
%   Detailed explanation goes here

for n = 1:popsize
    x = pop(n).x_chrom;
    y = pop(n).y_chrom;
    
    if (x < pb.x_range(1))
        pop(n).x_chrom = pb.x_range(1);
    elseif (x > pb.x_range(2))
        pop(n).x_chrom = pb.x_range(2);
    end
    
    if (y < pb.y_range(1))
        pop(n).y_chrom = pb.y_range(1);
    elseif (y > pb.y_range(2))
        pop(n).y_chrom = pb.y_range(2);
    end
end
corrected = pop;
end

