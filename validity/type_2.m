function corrected = type_2(pop, popsize, pb)
%TYPE_2 Summary of this function goes here
%   Detailed explanation goes here

for n = 1:popsize
    x = pop(n).x_chrom;
    y = pop(n).y_chrom;
    
    if (x < pb.x_range(1))
        pop(n).x_chrom = 2 * pb.x_range(1) - x;
    elseif (x > pb.x_range(2))
        pop(n).x_chrom = 2 * pb.x_range(2) - x;
    end
    
    if (y < pb.y_range(1))
        pop(n).y_chrom = 2 * pb.y_range(1) - y;
    elseif (y > pb.y_range(2))
        pop(n).y_chrom = 2 * pb.y_range(2) - y;
    end
end
corrected = pop;
end

