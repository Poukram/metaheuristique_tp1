function pop = rel_fitness(popsize, pop)
%REL_FITNESS Summary of this function goes here
%   Detailed explanation goes here
fitness_sum = sum([pop.fitness]);

% First compute pi
for n = 1:popsize
    pop(n).selec_proba = [ pop(n).fitness / fitness_sum , 0]; % as [pi , pci]
end

end

