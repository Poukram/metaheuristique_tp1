function pop = non_lineaire(popsize, pop)
%NON_LINEAIRE Summary of this function goes here
%   Detailed explanation goes here
[~, I] = sort([pop.fitness], 'ascend');
pop = pop(I);
alpha = 0.75; % high selective pressure

for n = 1:popsize
    pop(n).selec_proba = [alpha*((1 - alpha)^(popsize - n)), 0];
end
end