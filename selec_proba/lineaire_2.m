function pop = lineaire_2(popsize, pop)
%LINEAIRE_2 Summary of this function goes here
%   Detailed explanation goes here
[~, I] = sort([pop.fitness], 'descend');
pop = pop(I);
r = (2/(N*(N-1)))/4;
q = (r*(N-1))/2 + 1/N;

for n = 1:popsize
    pop(n).selec_proba = [q - (n - 1) * r, 0];
end
end