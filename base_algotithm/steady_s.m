function pop = steady_s(pop, popsize, param, pc, pm, pb, g_max, gen_nb)
%STEADY_S This function runs the genetic algorithm in steady state mode

res_tournoi = tournoi(pop, popsize);
p_ind = randi(popsize,2);
p1 = res_tournoi(p_ind(1));
p2 = res_tournoi(p_ind(2));

enfants = croisement(param.crossover, [p1 p2], 2, pc);
mutants = mutation(param.mutation, enfants, 2, pm, pb, gen_nb, g_max);
corrected = validity(param.v_checking, mutants, 2, pb);

to_kill = zeros(1,2);
t_size = 8;
t = zeros(t_size, popsize);

for n = 1:t_size
    t(n,:) = randperm(popsize);
end

% We only replace two individuals after choosing them with a tournament
% selection method among the less valuable individuals
for n = 1:2
    tmp(1, t_size) = Individual(0, 0);
    for k = 1:t_size
        tmp(k) = pop(t(k,n));
    end
    [~,I] = min([tmp.fitness]);
    to_kill(n) = t(I,n);
end
pop(to_kill(1)) = corrected(1);
pop(to_kill(2)) = corrected(2);
end

