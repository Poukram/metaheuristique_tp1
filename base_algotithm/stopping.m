function alive = stopping(param, ~, pop_history, gen_nb, g_max)
%STOPPING Determines which parameter to monitor to determine then
%alorithm's end of execution

std_threshold = 0.025;
rate_threshold = 0.05;
alive = 1;

switch param.critere_arret
    case "temps"
        if (gen_nb >= g_max)
            alive = 0;
        end
    case "fitness"
        tmp = [pop_history(gen_nb+1,:).fitness];
        res = std(tmp);
        if (res < std_threshold)
            alive = 0;
        end
    case "tx_chgt_fitness"
        m1 = max([pop_history(gen_nb,:).fitness]);
        m2 = max([pop_history(gen_nb+1,:).fitness]);
        rate = abs(m2-m1)/abs(m1);
        if (rate < rate_threshold)
            alive = 0;
        end
    otherwise
        warning("Invalid method to evalute stopping necessity");
end
end

