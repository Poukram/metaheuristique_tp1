function enfants = croisement(crossover, pop, popsize, pc)
%CROISEMENT This function determines which crossover operator to use

switch crossover
    case "single_pt"
        enfants = single_pt(pop, popsize, pc);
    case "multi_pt"
        enfants = multi_pt(pop, popsize, pc);
    case "uniform"
        enfants = uniform(pop, popsize, pc);
    case "whole_arithmetic"
        enfants = whole_arithmetic(pop, popsize, pc);
    case "local_arithmetic"
        enfants = local_arithmetic(pop, popsize, pc);
    case "blend"
        enfants = blend(pop, popsize, pc);
    case "simulated_bin"
        enfants = simulated_bin(pop, popsize, pc);
    case "laplace"
        enfants = laplace_c(pop, popsize, pc);
    otherwise
        warning("Unexpected crossover name, maybe not yet implemented");
end
end

