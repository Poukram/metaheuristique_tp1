function corrected = validity(validity, pop, popsize, pb)
%MUTATION Checks that all members of the population are valid points in the
%specified space or corrects them otherwise.

switch validity
    case "none"
        corrected = pop;
    case "type_1"
        corrected = type_1(pop, popsize, pb);
    case "type_2"
        corrected = type_2(pop, popsize, pb);
    otherwise
        warning("Unexpected validity checking method name, maybe not yet implemented");
end
end