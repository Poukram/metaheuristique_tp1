function res = genetic_a( popsize, g_max, g_stop, pc, pm, param, pb )
%GENETIC_A The main structure of the genetic algorithm
%   Chooses the operators to call depending on user settings
%   and returns a structure containing all history data to plot

%--------------Init vars and detect if we use binary coding here----------
binary_coding = strcmp("single_pt", param.crossover) ||...
    strcmp("multi_pt", param.crossover) ||...
    strcmp("uniform", param.crossover) ||...
    strcmp("davis_order", param.crossover);

param.problem = pb;
chr_builder = ChromosomeBuilder(binary_coding);
pop_history(g_max+1, popsize) = Individual(0, 0);
gen_nb = 0;
alive = 1;
%-------------------------------------------------------------------------

pop = init_pop(popsize, binary_coding, pb, chr_builder);
pop = eval_fitness(chr_builder, param, binary_coding, pop, popsize);
pop_history(1,:) = pop;

while (alive && gen_nb < g_stop)
    if (param.steady_s == 1)
        corrected = steady_s(pop, popsize, param, pc, pm, pb, g_max, gen_nb);
    else
        mating_pool = selection(param, pop, popsize);
        enfants = croisement(param.crossover, mating_pool, popsize, pc);
        mutants = mutation(param.mutation, enfants, popsize, pm, pb, gen_nb, g_max);
        corrected = validity(param.v_checking, mutants, popsize, pb);
    end
    pop = eval_fitness(chr_builder, param, binary_coding, corrected, popsize);
    
    gen_nb = gen_nb + 1;
    pop_history(gen_nb+1,:) = pop;
    alive = stopping(param, popsize, pop_history, gen_nb, g_max);
end
res = struct;
res.hist = pop_history;
res.popsize = popsize;
res.gen_nb = gen_nb;
res.bc = binary_coding;
res.pb = pb;
res.chr_b = chr_builder;
end

