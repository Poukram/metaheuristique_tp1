function population = eval_fitness(chr_builder, param, binary_coding, pop, popsize )
%EVAL_FITNESS This functions evaluates and scales fitness
%   Fitness scaling depends on the algorithm's parameters
pb = param.problem;

for n = 1:popsize
    x = pop(n).x_chrom;
    y = pop(n).y_chrom;
    
    if binary_coding
        x_approx = chr_builder.bin_chromosome_decode(x, pb.x_range);
        y_approx = chr_builder.bin_chromosome_decode(y, pb.y_range);
        fitness = pb.fun(x_approx, y_approx);
    else
        fitness = pb.fun(x, y);
    end
    pop(n).obj_val = fitness;
    pop(n).fitness = fitness;
end

pop = fitness_transfer(pop, popsize, pb.pb_type);

switch param.scale
    case "lineaire"
        pop = linear_scale(pop, popsize);
    case "sigma"
        pop = sigma_trunc(pop, popsize);
end

population = pop;

end

