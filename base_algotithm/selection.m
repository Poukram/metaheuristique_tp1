function mating_pool = selection(param, pop, popsize)
%SELECTION This function handles the selection and building of the mating
%pool
%   It first computes the probability that an individual will be chosen for
%   mating then uses the given selection method to build a mating pool;

% First compute pi
switch param.proba_sel
    case "relative"
        pop = rel_fitness(popsize, pop); % compute the probability of being chosen
    case "lineaire_1"
        pop = lineaire_1(popsize, pop);
    case "lineaire_2"
        pop = lineaire_1(popsize, pop);
    case "non_lineaire"
        pop = non_lineaire(popsize, pop);
    otherwise
        warning("Warning unsupported selection probability method")
end

%Then compute pci
for n = 1:popsize
    pci = 0;
    if (n == 1)
        pci = pop(n).selec_proba(1);
    else
        for k = 1:n
            pci = pci + pop(k).selec_proba(1);
        end
    end
    pop(n).selec_proba(2) = pci;
end

% Then build mating pool
switch param.selection
    case "RWS"
        mating_pool = rws(pop, popsize);
    case "SUS"
        mating_pool = sus(pop, popsize);
    case "tournoi"
        mating_pool = tournoi(pop, popsize);
    otherwise
        warning("Bad selection mode, aborting")
end
end

