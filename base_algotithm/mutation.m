function mutants = mutation(mutation, pop, popsize, pm, pb, g, g_max)
%MUTATION This function determines which mutation operator to use

switch mutation
    case "bit_flip"
        mutants = bit_flip(pop, popsize, pm);
    case "uniform"
        mutants = uniform_m(pop, popsize, pm, pb);
    case "boundary"
        mutants = boundary_m(pop, popsize, pm, pb);
    case "nonuniform"
        mutants = nonuniform_m(pop, popsize, pm, pb, g, g_max);
    case "normal"
        mutants = normal_m(pop, popsize, pm);
    case "polynomial"
        mutants = polynomial_m(pop, popsize, pm, pb);
    otherwise
        warning("Unexpected mutation name, maybe not yet implemented");
end
end