function population = init_pop( popsize, binary_coding, pb, chr_builder )
%INIT_POP initializes the population for the genetic algorithm

pop(popsize) = Individual(0, 0);
for n = 1:popsize
    x = chr_builder.gen_chromosome(pb.x_range);
    y = chr_builder.gen_chromosome(pb.y_range);
    pop(n) = Individual(x, y);
end
 population = pop;
end
