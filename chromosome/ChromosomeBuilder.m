classdef ChromosomeBuilder
    %CHROMOSOME Generates new chromosomes and helps apporximating a binary
    %chromosome value
    
    properties
        binary_coding
    end
    
    methods
        function obj = ChromosomeBuilder(binary)
            %CHROMOSOME ONly needs to know if we're using binary
            %chromosomes
            obj.binary_coding = binary;
        end
        
        function res = gen_chromosome(obj, range)
            if (obj.binary_coding)
                res = rand(1, 12) < 0.5;
            else
                res = rand() * abs(range(2) - range(1)) + range(1);
            end
        end
    end
    methods(Static)
        % Formule slide 20 chapitre 2
        function res = bin_chromosome_decode(value, range)
            tmp = 0;
            for n = 12:-1:1
                tmp = tmp + 2^(n-1) * value(n);
            end
            res = (tmp / 2^12) * (range(2) - range(1)) + range(1);
        end
    end
end

