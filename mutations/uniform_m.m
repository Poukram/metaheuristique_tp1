function mutants = uniform_m(pop, popsize, pm, pb)
%UNIFORM_M Summary of this function goes here
%   Detailed explanation goes here 

for n = 1:popsize
    tmp = rand();
    if (tmp <= pm)
        pop(n).x_chrom = rand() * (pb.x_range(2) - pb.x_range(1)) + pb.x_range(1);
    end
    
    tmp = rand();
    if (tmp <= pm)
        pop(n).y_chrom = rand() * (pb.y_range(2) - pb.y_range(1)) + pb.y_range(1);
    end
end

mutants = pop;
end

