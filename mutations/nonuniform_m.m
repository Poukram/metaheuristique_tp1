function mutants = uniform_m(pop, popsize, pm, pb, g, g_max)
%UNIFORM_M Summary of this function goes here
%   Detailed explanation goes here 
b = 4;

    function res = delta(y)
        res = y * (1 - rand()^(((1-g)/g_max)^b));
    end

for n = 1:popsize
    tmp = rand();
    if (tmp <= pm)
        u = rand();
        x = pop(n).x_chrom;
        
        if (u < 0.5)
            pop(n).x_chrom = x - delta(x - pb.x_range(1));
        else
            pop(n).x_chrom = x + delta(pb.x_range(2) - x);
        end
    end
    
    tmp = rand();
    if (tmp <= pm)
        u = rand();
        y = pop(n).y_chrom;
        
        if (u < 0.5)
            pop(n).y_chrom = y - delta(y - pb.y_range(1));
        else
            pop(n).y_chrom = y + delta(pb.y_range(2) - y);
        end
    end
end

mutants = pop;
end

