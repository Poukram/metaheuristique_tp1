function mutants = boundary_m(pop, popsize, pm, pb)
%UNIFORM_M Summary of this function goes here
%   Detailed explanation goes here 

for n = 1:popsize
    tmp = rand();
    if (tmp <= pm)
        u = rand();
        if (u <= 0.5)
            pop(n).x_chrom = pb.x_range(1);
        else
            pop(n).x_chrom = pb.x_range(2);
        end
    end
    
    tmp = rand();
    if (tmp <= pm)
        u = rand();
        if (u <= 0.5)
            pop(n).y_chrom = pb.y_range(1);
        else
            pop(n).y_chrom = pb.y_range(2);
        end
    end
end

mutants = pop;
end

