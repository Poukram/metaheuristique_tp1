function mutants = normal_m(pop, popsize, pm)
%UNIFORM_M Summary of this function goes here
%   Detailed explanation goes here 
sigma = 0.4;

for n = 1:popsize
    tmp = rand();
    if (tmp <= pm)
        pop(n).x_chrom = pop(n).x_chrom + sigma * normrnd(0,1);
    end
    
    tmp = rand();
    if (tmp <= pm)
        pop(n).y_chrom = pop(n).y_chrom + sigma * normrnd(0,1);
    end
end

mutants = pop;
end

