function mutants = bit_flip(pop, popsize, pm)
%BIT_FLIP Summary of this function goes here
%   Detailed explanation goes here 

for n = 1:popsize
    % First chromosome mutations
    for k = 1:12
        tmp = rand();
        if (tmp <= pm)
            if (pop(n).x_chrom(k) == 0)
                pop(n).x_chrom(k) = 1;
            else
                pop(n).x_chrom(k) = 0;
            end
        end
    end
    
    % Second chromosome mutations
    for k = 1:12
        tmp = rand();
        if (tmp <= pm)
            if (pop(n).y_chrom(k) == 0)
                pop(n).y_chrom(k) = 1;
            else
                pop(n).y_chrom(k) = 0;
            end
        end
    end
end

mutants = pop;
end

