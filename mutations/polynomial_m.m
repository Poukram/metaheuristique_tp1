function mutants = polynomial_m(pop, popsize, pm, pb)
%UNIFORM_M Summary of this function goes here
%   Detailed explanation goes here 
delta_m = 1;
ctrl = 5;

    function xi = xi_compute()
        u = rand();
        if (u < 0.5)
            xi = (2 * u)^(1/(ctrl + 1)) - 1;
        else
            xi = 1 - (2 * (1 - u))^(1/(ctrl + 1));
        end
    end

for n = 1:popsize
    tmp = rand();
    if (tmp <= pm)
        pop(n).x_chrom = pop(n).x_chrom + delta_m * xi_compute();
    end
    
    tmp = rand();
    if (tmp <= pm)
        pop(n).y_chrom = pop(n).y_chrom + delta_m * xi_compute();
    end
end

mutants = pop;
end

