%------------Configuring the path and initializing -------------------
clear variables
close all
clc
addpath(genpath(pwd))
crossover_type = ["single_pt", "multi_pt", "uniform", "laplace", ...
    "whole_arithmetic", "local_arithmetic", "blend", "simulated_bin"];
mutation_type = ["bit_flip", "uniform", "boundary", "nonuniform", "normal", "polynomial"];
validity_checking = ["none", "type_1", "type_2"];
sel_prob_type = ["relative", "lineaire_1", "lineaire_2", "non_lineaire"];
selection_type = ["RWS", "SUS", "tournoi"];
chgt_echelle_type = ["none", "lineaire", "sigma"];
stopping_type = [ "temps", "fitness", "tx_chgt_fitness"];
problem_name = ["rosenbrock", "griewank"];

%------------Genetic Algorithm Parameters-----------
operators = struct;
operators.crossover = crossover_type(6);
operators.mutation = mutation_type(4);
operators.proba_sel = sel_prob_type(4);
operators.selection = selection_type(3);
operators.scale = chgt_echelle_type(3);
operators.critere_arret = stopping_type(2);
operators.v_checking = validity_checking(3);
operators.steady_s = 0; % 1 to use, 0 otherwise

problem = TwoVarPb(problem_name(1));

params = struct;
params.popsize = 50;
params.g_max = 20;
params.g_stop = 300;
params.pc = 0;
params.pm = 0;

op = repmat(operators,1,6);
p = repmat(params,1,4);
res = test_builder(op, p);
%test_runner(res(1), res(2)); % uncomment to run the test suite

popsize = 40; % The number of individuals
g_max = 200; % The maximum number of generations
g_stop = 2000; % If we reach this generation nmber we stop no matter what
pc = 0.75; % The probability of a crossover to happen
pm = 0.075; % The probability of a mutation to happen


%------------Call to the genetic algorithm-----------
res = genetic_a(popsize, g_max, g_stop, pc, pm, operators, problem);
plot_results(res.hist, res.popsize, res.gen_nb, res.bc, res.pb, res.chr_b);