function mating_pool = tournoi(pop, popsize)
%TOURNOI Summary of this function goes here
%   Detailed explanation goes here
mating_pool(popsize) = Individual(0, 0);
t_size = 8;
t = zeros(t_size, popsize);

for n = 1:t_size
    t(n,:) = randperm(popsize);
end

for n = 1:popsize
    tmp(1, t_size) = Individual(0, 0);
    for k = 1:t_size
        tmp(k) = pop(t(k,n));
    end
    [~,I] = max([tmp.fitness]);
    mating_pool(n) = tmp(I);
end
end

