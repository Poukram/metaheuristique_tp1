function mating_pool = rws(pop, popsize)
%RWS Summary of this function goes here
%   Detailed explanation goes here
mating_pool(popsize) = Individual(0, 0);

selected = Individual(0, 0);

for n = 1:popsize
    tmp = rand();
    k = 1;

    if (tmp == 1)
        selected = pop(popsize);
        continue;
    end
    
    while ( k <= popsize)
        if (k == 1 && pop(k).selec_proba(2) > tmp)
            selected = pop(1);
            break;
        elseif (k > 1 && pop(k-1).selec_proba(2) <= tmp && pop(k).selec_proba(2) > tmp)
            selected = pop(k);
            break;
        end
        k = k + 1;
        
        if (k > popsize)
            break;
        end
    end
    
    mating_pool(n) = selected;            
end

end

