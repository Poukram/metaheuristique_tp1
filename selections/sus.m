function mating_pool = sus(pop, popsize)
%SUS Summary of this function goes here
%   Detailed explanation goes here
mating_pool(popsize) = Individual(0, 0);
nb_pointers = 4;
selected = Individual(0, 0);

for n = 1:popsize/nb_pointers
    u = rand()/nb_pointers;
    for i = 1:nb_pointers
        tmp = u*i;
        k = 1;
        
        if (tmp == 1)
            selected = pop(popsize);
            continue;
        end
        
        while ( k <= popsize)
            if (k == 1 && pop(k).selec_proba(2) > tmp)
                selected = pop(1);
                break;
            elseif (k > 1 && pop(k-1).selec_proba(2) <= tmp && pop(k).selec_proba(2) > tmp)
                selected = pop(k);
                break;
            end
            k = k + 1;
            
            if (k > popsize)
                warning("Could not find a valid individual")
                break;
            end                        
        end
        count = (n - 1) * nb_pointers + i;
        if (count > popsize)
            break;
        end
        mating_pool(count) = selected; 
    end
end

end

